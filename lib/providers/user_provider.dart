import 'package:flutter/foundation.dart';

class UserProvider extends ChangeNotifier {
    String? _accessToken;
    int? _userId;

    UserProvider([ this._accessToken, this._userId ]);

    get accessToken => _accessToken;
    get userId => _userId;

    setAccessToken(String? value) {
        _accessToken = value;
        notifyListeners();
    }

    setUserId(int? value) {
        _userId = value;
        notifyListeners();
    }
}