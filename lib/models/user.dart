class User {
    late final int? id;
    late final String? email;
    late final String? accessToken;

    User({ 
        this.id, 
        this.email,
        this.accessToken
    });

    factory User.fromJson(Map<String, dynamic> json) {
        return User(
            id: json['id'],
            email: json['email'],
            accessToken: json['accessToken']
        );
    }
}