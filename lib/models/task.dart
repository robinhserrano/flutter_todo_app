class Task {
    late final int id;
    late final int userId;
    late final String description;
    late final String? imageLocation;
    late final int isDone;

    Task({ 
        required this.id, 
        required this.userId,
        required this.description,
        required this.imageLocation,
        required this.isDone
    });

    factory Task.fromJson(Map<String, dynamic> json) {
        return Task(
            id: json['id'],
            userId: json['userId'],
            description: json['description'],
            imageLocation: json['imageLocation'],
            isDone: json['isDone']
        );
    }
}