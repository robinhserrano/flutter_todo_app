import 'package:freezed_annotation/freezed_annotation.dart';

//The rest of the contents for user.dasrt can be
//found in user.freezed.dart using the 'part' keyword.
part 'user.freezed.dart';
part 'user.g.dart';
//The @freezed annotation tells the build_runner to 
//create a freezed class named _$User in user.freezed.dart.
@freezed

class User with _$User{
    const factory User({
        int? id,
        String? email,
        String? accessToken
    }) = _User;

    factory User.fromJson(Map<String,dynamic> json) => _$UserFromJson(json);
}

