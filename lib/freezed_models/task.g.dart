// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Task _$_$_TaskFromJson(Map<String, dynamic> json) {
  return _$_Task(
    id: json['id'] as int,
    userId: json['userId'] as int,
    description: json['description'] as String,
    imageLocation: json['imageLocation'] as String?,
    isDone: json['isDone'] as int,
  );
}

Map<String, dynamic> _$_$_TaskToJson(_$_Task instance) => <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'description': instance.description,
      'imageLocation': instance.imageLocation,
      'isDone': instance.isDone,
    };
