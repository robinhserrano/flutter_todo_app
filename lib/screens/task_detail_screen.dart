import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '/utils/api.dart';
import '/freezed_models/task.dart';
import '/providers/user_provider.dart';
//import 'package:flutter/foundation.dart' show kIsWeb;


class TaskDetailScreen extends StatefulWidget {
    final Task _task;

    TaskDetailScreen(this._task);

    @override
    TaskDetailScreenState createState() => TaskDetailScreenState();
}

class TaskDetailScreenState extends State<TaskDetailScreen> {
    Future<Task>? _futureImageUpload;

    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000)
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget btnUpload = Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
                child: Text('Upload Image'),
                onPressed: () async {             
                    var selectedImage = await ImagePicker().pickImage(source: ImageSource.gallery);
                    setState(() {
                        if(selectedImage !=null){
                            print(selectedImage);
                            _futureImageUpload = API(accessToken).addTaskImage(
                                id: widget._task.id, 
                                filePath: selectedImage.path
                            ).catchError((error){
                                showSnackBar(context, error.toString());
                            }) as Future<Task>?;
                        }
                    });
                }
            )
        );

        Widget imageView = FutureBuilder(
            future: _futureImageUpload,
            builder: (context, snapshot) {
                return Container();
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Task Detail')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            Text(widget._task.description),
                            btnUpload,
                            imageView
                        ]
                    )
                )
            )
        );
    }
}