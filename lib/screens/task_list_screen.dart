//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/utils/api.dart';
import '/freezed_models/task.dart';
import '/widgets/add_task_dialog.dart';
import '/providers/user_provider.dart';
import '/widgets/task_item_tile.dart';



class TaskListScreen extends StatefulWidget {
  @override
  _TaskListScreenState createState() => _TaskListScreenState();
}

class _TaskListScreenState extends State<TaskListScreen> {
    Future<List<Task>> ? _futureTasks;
    void showAddTaskDialog(context){
        showDialog(
            context: context, 
            builder: (BuildContext context) => AddTaskDialog()
        ).then((value){
            final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
            setState(() {
                _futureTasks = API(accessToken).getTasks().catchError((error){
                    showSnackBar(context, error.toString());
                }) as Future<List<Task>>?;
            });
        });
    }

    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000)
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }

    Widget showTasks(List? tasks){
        var cltTasks = tasks!.map((task) => TaskItemTile(task)).toList();
        return ListView(
            children: cltTasks,
        );
    }
    
    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            setState(() {
                _futureTasks = API(accessToken).getTasks().catchError((error) {
                    showSnackBar(context, error.message);
                }) as Future<List<Task>>?;
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, snapshot){
                if (snapshot.hasData){
                    return showTasks(snapshot.data as List);
                }
                return Center(
                    child: CircularProgressIndicator(),
                );
            },
        );
        final String? accessToken = context.read<UserProvider>().accessToken;
        final int? userId = context.read<UserProvider>().userId;

        print('Printing from TaskListScreen...');
        print(accessToken);
        print(userId);
    
    return Scaffold(
        appBar: AppBar(title: Text('Todo List'),),
        drawer: Drawer(
            child: ListView(
                children: [
                    ListTile(
                        hoverColor: Color.fromRGBO(255, 212, 71, 1),
                        title: Text('Logout'),
                        onTap: () async{
                            Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                            Provider.of<UserProvider>(context, listen: false).setUserId(null);

                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.remove('accessToken');
                            prefs.remove('userId');

                            Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                        },
                    )
                ],
            ),
        ),
        body: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16.0),
            child: taskListView
            ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Color.fromRGBO(255, 212, 71, 1),
            onPressed: (){ 
                showAddTaskDialog(context);
            },
        ),
    );
  }
}