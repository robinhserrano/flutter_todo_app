import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '/utils/api.dart';
import '/providers/user_provider.dart';
import '/freezed_models/user.dart'; //edited

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    Future<User>? _futureLogin;
    
    final _formKey = GlobalKey<FormState>();
    final TextEditingController _tffEmailController = TextEditingController();
    final TextEditingController _tffPasswordController = TextEditingController();

    void login(BuildContext context){
        setState(() {
          _futureLogin = API().login(
              email: _tffEmailController.text,
              password: _tffPasswordController.text
          ).catchError((error){
              showSnackBar(context, error.toString());
          }) as Future<User>?; //edited
        });
    }

    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000)
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }

    @override
    Widget build(BuildContext context) {
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setUserId = Provider.of<UserProvider>(context, listen: false).setUserId;

        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            validator: (email){
                if(email==null || email.isEmpty){
                    return 'The email must be provided.';
                }else if(EmailValidator.validate(email) == false){
                    return 'A valid email must be provided.';
                }
            },
        );

    Widget tffPassword = TextFormField(
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        controller: _tffPasswordController,
        validator: (password){
            bool isPasswordValid = password!=null && password.isNotEmpty;
            return (isPasswordValid) ? null : 'The password must be provided.';
        },
    );

    Widget btnSubmit = Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: 8.0),
        child: ElevatedButton(
            child: Text("Login"),
            onPressed: (){
                if(_formKey.currentState!.validate()){
                    login(context);
                }
                else{
                    print('the login form is not valid');
                }
            },
        ),
    );

    Widget btnGoToRegister = Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: 8.0),
        child: ElevatedButton(
            child: Text("No Account Yet?"),
            onPressed: () {
                Navigator.pushNamed(context, '/register');
            },
        ),
    );

    Widget formLogin = Form(
        key: _formKey,
        child: Column(
            children: [
                tffEmail,
                tffPassword,
                btnSubmit,
                btnGoToRegister
            ],
        ),
    );

    Widget loginView = FutureBuilder(
        future: _futureLogin,
        builder: (context, snapshot){
            if(_futureLogin == null){
                return formLogin;
            }else if(snapshot.hasError == true){
                return formLogin;
            }else if(snapshot.hasData == true){
                Timer(Duration(milliseconds: 1),() async{
                    final prefs = await SharedPreferences.getInstance();
                    User user = snapshot.data as User;
                    
                    setUserId(user.id);
                    setAccessToken(user.accessToken);

                    prefs.setString('accessToken', user.accessToken!);
                    prefs.setInt('userId', user.id!);

                    Navigator.pushReplacementNamed(context, '/task-list');
                });
                return Container();
            }
                return Center(
                    child: CircularProgressIndicator()
                );
            }
    );

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(title: Text('Todo Login'),),
        body: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16.0),
            child: loginView
            // Column(
            //     children: [
            //         formLogin,
            //         btnSubmit,
            //         btnGoToRegister
            //     ]),
            ),
        );
    }
}