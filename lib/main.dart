import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io' show Platform;
import 'package:flutter/foundation.dart' show kIsWeb;

import '/screens/login_screen.dart';
import '/screens/register_screen.dart';
import '/screens/task_list_screen.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import '/providers/user_provider.dart';

Future<void> main() async{
    if (kIsWeb) {
        await dotenv.load(fileName: "assets/env/.env_web");
    }else if (Platform.isAndroid||Platform.isIOS) {
       await dotenv.load(fileName: "assets/env/.env_android");
    } 

    //Initial check for user's acess token from SharedPreferences.
    //Determine the initial route of app dependeding on existence of user's access token.
    WidgetsFlutterBinding.ensureInitialized();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //prefs.remove("accessToken");
    String? accessToken = prefs.getString("accessToken");
    String initialRoute = (accessToken != null) ? '/task-list' : '/';
    runApp(App(accessToken, initialRoute));
}

class App extends StatelessWidget {
  final String? _accessToken;
  final String _initialRoute;

  App(this._accessToken, this._initialRoute);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (BuildContext context) =>UserProvider(_accessToken),
        child: MaterialApp(
            theme: ThemeData(
                primaryColor: Color.fromRGBO(205,23,25, 1),
                elevatedButtonTheme: ElevatedButtonThemeData(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(255, 212, 71, 1),
                        onPrimary: Colors.black
                    )
                )
            ),
            initialRoute: this._initialRoute,
            routes: {
                '/': (context) => LoginScreen(),
                '/register': (context) => RegisterScreen(),
                '/task-list': (context) => TaskListScreen()
                },
            ),
        );
    }
}