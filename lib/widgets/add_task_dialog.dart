import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/api.dart';
import '/providers/user_provider.dart';


class AddTaskDialog extends StatefulWidget {
    @override
    _AddTaskDialogState createState() => _AddTaskDialogState();
}

class _AddTaskDialogState extends State<AddTaskDialog> {
    final tffDescriptionController = TextEditingController();

    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000)
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
    
    void addTask(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        
        API(accessToken).addTask(
            description:tffDescriptionController.text
        ).catchError((error){
            showSnackBar(context, error.toString());
        });
    }

    @override
    Widget build(BuildContext context) {
        return AlertDialog(
            title: Text('Add New Task'),
            content: Container(
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        TextFormField(
                            decoration: InputDecoration(labelText: 'Task Description'),
                            keyboardType: TextInputType.text,
                            controller: tffDescriptionController
                        )
                    ]
                )
            ),
            actions: [
                ElevatedButton(
                    onPressed: () {
                        addTask(context);
                        Navigator.of(context).pop();
                    },
                    child: Text('Add'),
                ),
                ElevatedButton(
                    onPressed: () {
                        Navigator.of(context).pop();
                    },
                    child: Text('Cancel'),
                ),
            ],
        );
    }
}
